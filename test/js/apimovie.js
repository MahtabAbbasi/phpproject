function loadMovieData()
{
  'use strict';

  console.log("in weather data document");

  var $newFirstDiv = $('#newFirstDiv');

  $newFirstDiv.after('<div id="weatherForecast"></div>');

  var $weatherForecast = $('#weatherForecast');

  $weatherForecast.prepend('<h4 id="weatherHeader">Weather Forecast of Montreal for the next 5 days </br> </br> </h4>');

  $('#weatherForecast').append('<ul id="weatherList"></ul>');
  var $listElem= $('#weatherList');

  //var weatherMontrealURL = 'http://dataservice.accuweather.com/forecasts/v1/daily/5day/56186?apikey=DbaM8Hj46KryKJ04QG5FgtgruX6rIkq5&metric=true';
//  var weatherMontrealURL = 'https://api.themoviedb.org/3/movie/550?api_key=c5c15796d86db777d5236b8acca9a3bb';
  var weatherMontrealURL = 'https://api.themoviedb.org/3/search/movie/Bumblebee?api_key=c5c15796d86db777d5236b8acca9a3bb&language=en-US&page=1&include_adult=false';
  
  $.getJSON(weatherMontrealURL, function(data)
  {
  //  var weatherData = data.DailyForecasts;
    var weatherData = data;
    console.log(weatherData);

      var weather = data.title;

      $listElem.append('<li class="day">' + weather + '</li>');
  });

} // End of loadWeatherData() function.
