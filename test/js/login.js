// Script Project - login.js

function validateForm()
{
  'use strict';

	console.log("in validate form");

  // Get references to the form elements:
  var emailAdd    = $('#email');
  var password    = $('#password');
  var newFirstDiv = $('#newFirstDiv');

  var isError = false;

  var secondDiv = $('#newSecondDiv');
  if(secondDiv.length != 0)
    if(newFirstDiv.find(secondDiv))
        secondDiv.remove();

  var str = "Error! Please complete the form!";

  // var amp = /^$/;
  // if(emailAdd.value.match(amp))
  if(emailAdd.val().length == 0)
  {
    str += "<br />  * Email address must be filled in!";

    isError = true;
  }

  // var at  = /@/;
  // if(!(emailAdd.value.match(at)))
  if(!(emailAdd.val().indexOf('@') > 0))
  {
    str += "</br> * Email address must contain the @ symbol!";

    isError = true;
  }

  // var len  = /^.{0,5}$/;
  // if(password.value.match(len))
  if(password.val().length < 6)
  {
    str += "</br> * Password length must be at least 6 characters!";

    isError = true;
  }

	if (isError == false)
  {
		return true;
	}
	else
  {
    var result = str.fontcolor("Red");

    var newDiv = $('<div id="newSecondDiv"></div>');
    newDiv.append(result);

    newFirstDiv.before(newDiv);

		console.log("returned false: do not submit");

		return false;
	}

} // End of validateForm() function.

// Function called when the window has been loaded.
// Function needs to add an event listener to the form.
function init()
{
  'use strict';

  //Read on: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Strict_mode

  $('#loginForm').css('width', '25em');

  var fieldset = $('fieldset');

  var newdiv = $('<div id="newFirstDiv"></div>');

  var str = "Today\'s Message: ";
  str += "\"in init\"".fontcolor("DarkGreen");
  str += "</br>Today\'s date: " + new Date().toDateString();
  str += "</br>Time now: " + new Date().toLocaleTimeString('en-US');

  newdiv.append(str);

  fieldset.append(newdiv);

  loadWeatherData();

  if (document && document.getElementById)
  {
   console.log("in init document");

   loginForm.onsubmit = validateForm;

  }

} // End of init() function.

// Assign an event listener to the window's load event:
$(document).ready(init);
