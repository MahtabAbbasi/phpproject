// Script Project - weather.js

function loadWeatherData()
{
  'use strict';

	console.log("in weather data document");

  var $newFirstDiv = $('#newFirstDiv');

  $newFirstDiv.after('<div id="weatherForecast"></div>');

  var $weatherForecast = $('#weatherForecast');

  $weatherForecast.prepend('<h4 id="weatherHeader">Weather Forecast of Montreal for the next 5 days </br> </br> </h4>');

  $('#weatherForecast').append('<ul id="weatherList"></ul>');
  var $listElem= $('#weatherList');

  var weatherMontrealURL = 'http://dataservice.accuweather.com/forecasts/v1/daily/5day/56186?apikey=DbaM8Hj46KryKJ04QG5FgtgruX6rIkq5&metric=true';
  $.getJSON(weatherMontrealURL, function(data)
  {
    var weatherData = data.DailyForecasts;
    console.log(weatherData);

    for (var i = 0; i< weatherData.length; i++)
    {
      var weather = weatherData[i];
      var date = weather.Date.toString().split('T')[0];
      var time = weather.Date.toString().split('T')[1].split('-')[0];
      var maxTemp = weather.Temperature.Maximum.Value;
      var minTemp = weather.Temperature.Minimum.Value;
      var day = weather.Day.ShortPhrase;
      var night = weather.Night.ShortPhrase;

      $listElem.append('<li class="day '+ i + '">' + "Weather data for:"
                      + '</br>' + "Date: "+ date + '</br>' + "Time: "
                      + time + '</br>'+ "Maximum Temperature: "
                      + maxTemp + " C" + '</br>'+ "Minimum Temperature: "
                      + minTemp + " C" + '</br>'+ "Day Weather Condition: "
                      + day + '</br>'+ "Night Weather Condition: "
                      + night + '</br></br>' + '</li>');
    }
  });

} // End of loadWeatherData() function.
