<?php

session_start();
require_once 'vendor/autoload.php';
require_once 'fpdf/fpdf.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

DB::$user = 'movieticketing';
DB::$dbName = 'movieticketing';
DB::$password = 'JxIRPIJDD6hmUbMx';
DB::$port = 3333;
DB::$host = 'localhost';
DB::$encoding = 'utf8';

//DB::$error_handler = 'db_error_handler';

function db_error_handler($params) {
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    $app->render('fatal_error.html.twig');
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
        ));

$view = $app->view();
$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);
$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

$app->get('/', function() use ($app) {
    $app->render('home.html.twig');
});

$app->get('/moviedescription/:id', function($id) use ($app) {
    $movieId = $id;
    $movie = DB::queryFirstRow("SELECT * FROM movies WHERE movieId=%i", $movieId);
    $_SESSION['movie'] = $movie;
    $cinemaList = DB::query("SELECT cinemas.name FROM "
                    . "movieshowtimes JOIN auditoriums on movieshowtimes.AuditoriumId = auditoriums.AudId "
                    . "JOIN cinemas on cinemas.cinemaId = auditoriums.cinemaId "
                    . "WHERE movieshowtimes.movieId=%i GROUP BY cinemas.cinemaId", $_SESSION['movie']['movieId']);
    $cinemaNames = array();
    foreach ($cinemaList as $cinema) {
        foreach ($cinema as $row) {
            array_push($cinemaNames, $row);
        }
    }
    $app->render('moviedescription.html.twig', array('movie' => $movie, 'cinemaList' => $cinemaNames));
});

$app->post('/moviedescription/:id', function($id) use ($app) {
    $selectedCinemaName = $app->request()->post('selectedcinema');
    $selectedCinema = DB::queryFirstRow("SELECT * FROM cinemas WHERE cinemas.name=%s", $selectedCinemaName);
    $_SESSION['cinema'] = $selectedCinema;
    $selectedDate = $app->request()->post('selecteddate');
    $_SESSION['date'] = $selectedDate;
    $showTimeList = DB::query("SELECT * FROM movieshowtimes JOIN auditoriums on "
                    . "movieshowtimes.AuditoriumId = auditoriums.AudId JOIN "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId JOIN "
                    . "movies on movies.movieId = movieshowtimes.movieId "
                    . "WHERE movies.movieId=%s and cinemas.cinemaId = %s and movieshowtimes.date = %s"
                    , $id, $selectedCinema['cinemaId'], $selectedDate);
    $auditorium = DB::queryFirstRow("SELECT * FROM auditoriums join "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId"
                    . " WHERE cinemas.cinemaId = %s", $_SESSION['cinema']['cinemaId']);
    $_SESSION['auditorium'] = $auditorium;
    $audRows = DB::query("SELECT * FROM audrows WHERE AudId=%i", $auditorium['AudId']);
    $app->render('showtimes.html.twig', array('showTimeList' => $showTimeList));
});

$app->get('/seatcharts', function() use ($app) {

    $showtimeId = $app->request()->get('showtime');
    $showtime = DB::queryFirstRow("SELECT * FROM movieshowtimes WHERE movieshowtimes.movieShowtimeId=%s ", $showtimeId);
    $_SESSION['showtime'] = $showtime;
    $auditorium = DB::queryFirstRow("SELECT * FROM auditoriums join "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId"
                    . " WHERE cinemas.cinemaId = %s", $_SESSION['cinema']['cinemaId']);
    $_SESSION['auditorium'] = $auditorium;
    $audRows = DB::query("SELECT * FROM audrows JOIN "
                    . "movieshowtimes on movieshowtimes.movieShowtimeId=audrows.showtimeid "
                    . "WHERE movieshowtimes.movieShowtimeId=%s"
                    , $showtime['movieShowtimeId']);
    $app->render('seatchart.html.twig', array('auditorium' => $auditorium, 'audRows' => $audRows));
});


$app->get('/ticketdetails', function() use ($app) {

    $selectedseat = $app->request()->get('seatNumber');
    $_SESSION['selectedseat'] = $selectedseat;
    $app->render('ticketdetails.html.twig', array(
        'movietitle' => $_SESSION['movie']['title'],
        'cinemaname' => $_SESSION['cinema']['name'],
        'auditorium' => $_SESSION['auditorium']['location'],
        'showdate' => $_SESSION['showtime']['date'],
        'showtime' => $_SESSION['showtime']['time'],
        'seatnumber' => substr_replace($selectedseat, "", -1)
    ));
});

$app->post('/ticketdetails', function() use ($app) {
    // gray seat in database
    $app->render('reserve_confirm.html.twig', array(
        'movietitle' => $_SESSION['movie']['title'],
        'cinemaname' => $_SESSION['cinema']['name'],
        'auditorium' => $_SESSION['auditorium']['location'],
        'showdate' => $_SESSION['showtime']['date'],
        'showtime' => $_SESSION['showtime']['time'],
        'seatnumber' => substr_replace($_SESSION['selectedseat'], "", -1)
    ));
});



$app->get('/pdf', function() use ($app) {

    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->Rect(20, 20, 170, 170, 'B');
    $pdf->SetFont('Arial', 'B', 18);
    $pdf->SetXY(80, 20);
    $pdf->Cell(80, 20, 'Ticket Information', 'C');
    $pdf->Ln(10);
    $pdf->Cell(50, 20, 'Name:', 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 20, 'Movie:' . $_SESSION['movie']['title'], 5, 20, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Cinema:' . $_SESSION['cinema']['name'], 5, 5, 'C');
    $pdf->Ln(10);
    //$pdf->SetX(45);
    $pdf->Cell(50, 50, 'Auditorium: ' . $_SESSION['auditorium']['location'], 5, 5, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Date: ' . $_SESSION['showtime']['date'], 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Show time: ' . $_SESSION['showtime']['time'], 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Seat Number: ' . substr_replace($_SESSION['selectedseat'], "", -1), 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->Output();
    exit;
});


$app->run();

