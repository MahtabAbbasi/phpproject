<?php

/* home.html.twig */
class __TwigTemplate_ef693196926baa65aaaa4c262284ee49721887de3db9de06eee4dab881509d5f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "home.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Index";
    }

    // line 5
    public function block_content($context, array $blocks = array())
    {
        // line 6
        echo "
<form method=\"post\">
    Auditorium id: <input type=\"text\" name=\"audId\"><br>
    movie id: <input type=\"text\" name=\"movieId\"><br>
    <input type=\"submit\" value=\"Reserve your seat!\">
    <input type=\"submit\" value=\"Movie description\">
</form>

";
    }

    public function getTemplateName()
    {
        return "home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  38 => 6,  35 => 5,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Index{% endblock %}

{% block content %}

<form method=\"post\">
    Auditorium id: <input type=\"text\" name=\"audId\"><br>
    movie id: <input type=\"text\" name=\"movieId\"><br>
    <input type=\"submit\" value=\"Reserve your seat!\">
    <input type=\"submit\" value=\"Movie description\">
</form>

{% endblock %}
", "home.html.twig", "C:\\xampp\\htdocs\\movieticketingtannaz\\templates\\home.html.twig");
    }
}
