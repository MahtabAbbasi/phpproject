<?php

/* ticketdetails.html.twig */
class __TwigTemplate_b7045e683a9b8fab6a48895f102767678046f547796127c9271e945ce0607bb5 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "ticketdetails.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Index";
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "
    <form method=\"post\">   
        movie name: <p>";
        // line 7
        echo twig_escape_filter($this->env, ($context["movietitle"] ?? null), "html", null, true);
        echo "</p><br>
        Cinema name:<p>";
        // line 8
        echo twig_escape_filter($this->env, ($context["cinemaname"] ?? null), "html", null, true);
        echo "</p><br>
        Auditorium: <p>";
        // line 9
        echo twig_escape_filter($this->env, ($context["auditorium"] ?? null), "html", null, true);
        echo "</p><br>
        Date:<p>";
        // line 10
        echo twig_escape_filter($this->env, ($context["showdate"] ?? null), "html", null, true);
        echo "</p><br>
        showtime:<p>";
        // line 11
        echo twig_escape_filter($this->env, ($context["showtime"] ?? null), "html", null, true);
        echo "</p><br>
        seat id: <p>";
        // line 12
        echo twig_escape_filter($this->env, ($context["seatnumber"] ?? null), "html", null, true);
        echo "</p><br>
        User Name:<p></p><br>
        <input type=\"submit\" value=\"Reserve\">
    </form>

";
    }

    public function getTemplateName()
    {
        return "ticketdetails.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  62 => 12,  58 => 11,  54 => 10,  50 => 9,  46 => 8,  42 => 7,  38 => 5,  35 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Index{% endblock %}
{% block content %}

    <form method=\"post\">   
        movie name: <p>{{movietitle}}</p><br>
        Cinema name:<p>{{cinemaname}}</p><br>
        Auditorium: <p>{{auditorium}}</p><br>
        Date:<p>{{showdate}}</p><br>
        showtime:<p>{{showtime}}</p><br>
        seat id: <p>{{seatnumber}}</p><br>
        User Name:<p></p><br>
        <input type=\"submit\" value=\"Reserve\">
    </form>

{% endblock %}

", "ticketdetails.html.twig", "C:\\xampp\\htdocs\\movieticketingtannaz\\templates\\ticketdetails.html.twig");
    }
}
