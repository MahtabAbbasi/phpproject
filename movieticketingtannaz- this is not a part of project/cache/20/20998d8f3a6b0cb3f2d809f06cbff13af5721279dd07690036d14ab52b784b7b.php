<?php

/* moviedescription.html.twig */
class __TwigTemplate_4d52cdb7c1ef1ff395fa5a4f071e49069e6c91af5530a6d083d5cde36826461d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "moviedescription.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Movie Description";
    }

    // line 4
    public function block_content($context, array $blocks = array())
    {
        // line 5
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 header\">
                <img id=\"logo\" src=\"/images/logo.jpg\" alt=\"1\">
            </div>
            <div class=\"col-md-3 header\">
                <input id=\"search\" type=\"search\" placeholder=\"Search\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 header\">
                <nav class=\"navbar navbar-expand-md  navbar-dark\">
                    <a class=\"navbar-brand\" href=\"#\"></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                        <span class=\"navbar-toggler-icon\"></span></button>
                    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                        <ul class=\"navbar-nav\">
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"register.html\">SignUp</a></li>
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"login.html\">Login</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-sm-8\">
                <div class=\"card\">
                    <img class=\"card-img-top\" src=\"/images/Bohemian-Rhapsody-banner_1.jpg\" alt=\"moviebanner\">
                    <div class=\"card-body\">
                        <h5 class=\"card-title\">";
        // line 34
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "title", array()), "html", null, true);
        echo "</h5>
                        <img  src=\"/images/imdb_logo.png\" alt=\"imdb rating\"><br>
                        <p id=\"movieuserrating\" class=\"card-text\">";
        // line 36
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "userrating", array()), "html", null, true);
        echo "</p>
                        <p id=\"movieinfo\" class=\"card-text\">";
        // line 37
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "duration", array()), "html", null, true);
        echo "|";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "category", array()), "html", null, true);
        echo "|";
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "movieRating", array()), "html", null, true);
        echo "</p>
                        <p id=\"moviedescription\" class=\"card-text\">";
        // line 38
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "description", array()), "html", null, true);
        echo "</p><br>
                        <button value=\"Trailer\" class=\"card-text\" href=\"";
        // line 39
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "trailer", array()), "html", null, true);
        echo "\">Watch Trailer</button>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"card\">
                    <div class=\"card-body\">
                        <h5 class=\"card-title\">TICKETS</h5>
                        <form method=\"post\">
                            <p id=\"selectedmovietitle\">";
        // line 48
        echo twig_escape_filter($this->env, $this->getAttribute(($context["movie"] ?? null), "title", array()), "html", null, true);
        echo "</p><br>
                            <select class=\"selectedcinema\"  name=\"selectedcinema\">
                                ";
        // line 50
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["cinemaList"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["cinema"]) {
            // line 51
            echo "                                ";
            $context["selected"] = "selected";
            // line 52
            echo "                                <option value=\"";
            echo twig_escape_filter($this->env, $context["cinema"], "html", null, true);
            echo "\" ";
            echo twig_escape_filter($this->env, ($context["selected"] ?? null), "html", null, true);
            echo ">";
            echo twig_escape_filter($this->env, $context["cinema"], "html", null, true);
            echo "</option>
                            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['cinema'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "                            </select><br>
                            <input type=\"date\" name=\"selecteddate\" id=\"selecteddate\" placeholder=\"Select a date\"> 
                            <input type=\"submit\" name=\"submitdatecinema\" class=\"submitdatecinema\" value=\"Check Available Showtimes\">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "moviedescription.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 54,  114 => 52,  111 => 51,  107 => 50,  102 => 48,  90 => 39,  86 => 38,  78 => 37,  74 => 36,  69 => 34,  38 => 5,  35 => 4,  29 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Movie Description{% endblock %}
{% block content %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 header\">
                <img id=\"logo\" src=\"/images/logo.jpg\" alt=\"1\">
            </div>
            <div class=\"col-md-3 header\">
                <input id=\"search\" type=\"search\" placeholder=\"Search\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 header\">
                <nav class=\"navbar navbar-expand-md  navbar-dark\">
                    <a class=\"navbar-brand\" href=\"#\"></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                        <span class=\"navbar-toggler-icon\"></span></button>
                    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                        <ul class=\"navbar-nav\">
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"register.html\">SignUp</a></li>
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"login.html\">Login</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-sm-8\">
                <div class=\"card\">
                    <img class=\"card-img-top\" src=\"/images/Bohemian-Rhapsody-banner_1.jpg\" alt=\"moviebanner\">
                    <div class=\"card-body\">
                        <h5 class=\"card-title\">{{movie.title}}</h5>
                        <img  src=\"/images/imdb_logo.png\" alt=\"imdb rating\"><br>
                        <p id=\"movieuserrating\" class=\"card-text\">{{movie.userrating}}</p>
                        <p id=\"movieinfo\" class=\"card-text\">{{movie.duration}}|{{movie.category}}|{{movie.movieRating}}</p>
                        <p id=\"moviedescription\" class=\"card-text\">{{movie.description}}</p><br>
                        <button value=\"Trailer\" class=\"card-text\" href=\"{{movie.trailer}}\">Watch Trailer</button>
                    </div>
                </div>
            </div>
            <div class=\"col-sm-4\">
                <div class=\"card\">
                    <div class=\"card-body\">
                        <h5 class=\"card-title\">TICKETS</h5>
                        <form method=\"post\">
                            <p id=\"selectedmovietitle\">{{movie.title}}</p><br>
                            <select class=\"selectedcinema\"  name=\"selectedcinema\">
                                {% for cinema in cinemaList %}
                                {% set selected = 'selected' %}
                                <option value=\"{{cinema}}\" {{ selected }}>{{cinema}}</option>
                            {% endfor %}
                            </select><br>
                            <input type=\"date\" name=\"selecteddate\" id=\"selecteddate\" placeholder=\"Select a date\"> 
                            <input type=\"submit\" name=\"submitdatecinema\" class=\"submitdatecinema\" value=\"Check Available Showtimes\">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}", "moviedescription.html.twig", "C:\\xampp\\htdocs\\movieticketingtannaz\\templates\\moviedescription.html.twig");
    }
}
