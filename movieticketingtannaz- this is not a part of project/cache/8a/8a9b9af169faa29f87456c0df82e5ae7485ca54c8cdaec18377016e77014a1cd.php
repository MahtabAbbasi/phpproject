<?php

/* seatchart.html.twig */
class __TwigTemplate_85ef363ec9199bcbbd4d3edf1bda3e36e920dc83471f9ea98cd3f0852fad5459 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "seatchart.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Movie Description";
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$('body').on('click', function (e) {
                var seatNumber = e.target.id;
                if ((seatNumber[seatNumber.length -1] == 'R') ||(seatNumber[seatNumber.length -1] == 'X'))
                {
                    alert(\"This seat is not available please select another seat\");
                    return;
                }
                
                window.location = \"/ticketdetails\" + \"?seatNumber=\" + seatNumber;
                //window.location = \"/ticketdetails/seat/\" + seatNumber;
            });
        });
    </script>
";
    }

    // line 22
    public function block_content($context, array $blocks = array())
    {
        // line 23
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 header\">
                <img id=\"logo\" src=\"/images/logo.jpg\" alt=\"1\">
            </div>
            <div class=\"col-md-3 header\">
                <input id=\"search\" type=\"search\" placeholder=\"Search\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 header\">
                <nav class=\"navbar navbar-expand-md  navbar-dark\">
                    <a class=\"navbar-brand\" href=\"#\"></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                        <span class=\"navbar-toggler-icon\"></span></button>
                    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                        <ul class=\"navbar-nav\">
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"register.html\">SignUp</a></li>
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"login.html\">Login</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-sm-12\">
                <div class=\"card\">
                    <div class=\"card-body\">
                        <div class=\"centeredContent\">
                            <p>Select your seat:</p>
                            ";
        // line 53
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["audRows"] ?? null));
        $context['loop'] = array(
          'parent' => $context['_parent'],
          'index0' => 0,
          'index'  => 1,
          'first'  => true,
        );
        if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
            $length = count($context['_seq']);
            $context['loop']['revindex0'] = $length - 1;
            $context['loop']['revindex'] = $length;
            $context['loop']['length'] = $length;
            $context['loop']['last'] = 1 === $length;
        }
        foreach ($context['_seq'] as $context["_key"] => $context["rows"]) {
            // line 54
            echo "                                ";
            $context["foo"] = twig_split_filter($this->env, $this->getAttribute($context["rows"], "seatsInRow", array()), ",");
            // line 55
            echo "                                ";
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable(($context["foo"] ?? null));
            $context['loop'] = array(
              'parent' => $context['_parent'],
              'index0' => 0,
              'index'  => 1,
              'first'  => true,
            );
            if (is_array($context['_seq']) || (is_object($context['_seq']) && $context['_seq'] instanceof Countable)) {
                $length = count($context['_seq']);
                $context['loop']['revindex0'] = $length - 1;
                $context['loop']['revindex'] = $length;
                $context['loop']['length'] = $length;
                $context['loop']['last'] = 1 === $length;
            }
            foreach ($context['_seq'] as $context["_key"] => $context["i"]) {
                echo " 
                                    ";
                // line 56
                if (($context["i"] == "1")) {
                    // line 57
                    echo "                                        <canvas class=\"canvas\" id=\"Row: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["loop"], "parent", array()), "loop", array()), "index", array()), "html", null, true);
                    echo " Seatnumber:";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                    echo "A\" width=\"30\" height=\"30\" style=\" background-image:url('../images/ava_seat.png')\"></canvas>
                                        ";
                } elseif ((                // line 58
$context["i"] == "0")) {
                    // line 59
                    echo "                                        <canvas class=\"canvas\" id=\"Row: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["loop"], "parent", array()), "loop", array()), "index", array()), "html", null, true);
                    echo " Seatnumber:";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                    echo "R\" width=\"30\" height=\"30\" style=\" background-image:url('../images/res_seat.png')\"></canvas>
                                        ";
                } elseif ((                // line 60
$context["i"] == "\$")) {
                    // line 61
                    echo "                                        <canvas class=\"canvas\" id=\"Row: ";
                    echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute($this->getAttribute($context["loop"], "parent", array()), "loop", array()), "index", array()), "html", null, true);
                    echo " Seatnumber:";
                    echo twig_escape_filter($this->env, $this->getAttribute($context["loop"], "index", array()), "html", null, true);
                    echo "X\" width=\"30\" height=\"30\" ></canvas>
                                        ";
                }
                // line 63
                echo "                                    ";
                ++$context['loop']['index0'];
                ++$context['loop']['index'];
                $context['loop']['first'] = false;
                if (isset($context['loop']['length'])) {
                    --$context['loop']['revindex0'];
                    --$context['loop']['revindex'];
                    $context['loop']['last'] = 0 === $context['loop']['revindex0'];
                }
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['i'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 64
            echo "                                <br>
                                <hr>
                            ";
            ++$context['loop']['index0'];
            ++$context['loop']['index'];
            $context['loop']['first'] = false;
            if (isset($context['loop']['length'])) {
                --$context['loop']['revindex0'];
                --$context['loop']['revindex'];
                $context['loop']['last'] = 0 === $context['loop']['revindex0'];
            }
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['rows'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 67
        echo "                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "seatchart.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  192 => 67,  176 => 64,  162 => 63,  154 => 61,  152 => 60,  145 => 59,  143 => 58,  136 => 57,  134 => 56,  114 => 55,  111 => 54,  94 => 53,  62 => 23,  59 => 22,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}Movie Description{% endblock %}
{% block addhead %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$('body').on('click', function (e) {
                var seatNumber = e.target.id;
                if ((seatNumber[seatNumber.length -1] == 'R') ||(seatNumber[seatNumber.length -1] == 'X'))
                {
                    alert(\"This seat is not available please select another seat\");
                    return;
                }
                
                window.location = \"/ticketdetails\" + \"?seatNumber=\" + seatNumber;
                //window.location = \"/ticketdetails/seat/\" + seatNumber;
            });
        });
    </script>
{% endblock %}
{% block content %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 header\">
                <img id=\"logo\" src=\"/images/logo.jpg\" alt=\"1\">
            </div>
            <div class=\"col-md-3 header\">
                <input id=\"search\" type=\"search\" placeholder=\"Search\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 header\">
                <nav class=\"navbar navbar-expand-md  navbar-dark\">
                    <a class=\"navbar-brand\" href=\"#\"></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                        <span class=\"navbar-toggler-icon\"></span></button>
                    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                        <ul class=\"navbar-nav\">
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"register.html\">SignUp</a></li>
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"login.html\">Login</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-sm-12\">
                <div class=\"card\">
                    <div class=\"card-body\">
                        <div class=\"centeredContent\">
                            <p>Select your seat:</p>
                            {% for rows in audRows %}
                                {% set foo = rows.seatsInRow|split(',') %}
                                {% for i in foo %} 
                                    {% if i == '1' %}
                                        <canvas class=\"canvas\" id=\"Row: {{loop.parent.loop.index}} Seatnumber:{{loop.index}}A\" width=\"30\" height=\"30\" style=\" background-image:url('../images/ava_seat.png')\"></canvas>
                                        {% elseif i == '0' %}
                                        <canvas class=\"canvas\" id=\"Row: {{loop.parent.loop.index}} Seatnumber:{{loop.index}}R\" width=\"30\" height=\"30\" style=\" background-image:url('../images/res_seat.png')\"></canvas>
                                        {% elseif  i == '\$' %}
                                        <canvas class=\"canvas\" id=\"Row: {{loop.parent.loop.index}} Seatnumber:{{loop.index}}X\" width=\"30\" height=\"30\" ></canvas>
                                        {% endif %}
                                    {% endfor %}
                                <br>
                                <hr>
                            {% endfor %}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}
", "seatchart.html.twig", "C:\\xampp\\htdocs\\movieticketingtannaz\\templates\\seatchart.html.twig");
    }
}
