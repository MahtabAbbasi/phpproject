<?php

/* showtimes.html.twig */
class __TwigTemplate_ef31d43398f9a17e2f0e72cc0bebe204a1635089fa94ea60bc3f0bb152199a85 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("master.html.twig", "showtimes.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "master.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "ShowTimes";
    }

    // line 4
    public function block_addhead($context, array $blocks = array())
    {
        // line 5
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$('body').on('click', function (e) {
                var showtime = e.target.id;
                window.location = \"/seatcharts\" + \"?showtime=\" + showtime;
            });
        });
    </script>
";
    }

    // line 15
    public function block_content($context, array $blocks = array())
    {
        // line 16
        echo "    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 header\">
                <img id=\"logo\" src=\"/images/logo.jpg\" alt=\"1\">
            </div>
            <div class=\"col-md-3 header\">
                <input id=\"search\" type=\"search\" placeholder=\"Search\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 header\">
                <nav class=\"navbar navbar-expand-md  navbar-dark\">
                    <a class=\"navbar-brand\" href=\"#\"></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                        <span class=\"navbar-toggler-icon\"></span></button>
                    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                        <ul class=\"navbar-nav\">
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"register.html\">SignUp</a></li>
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"login.html\">Login</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-sm-12\">
                <div class=\"card\">
                    <div class=\"card-body\">
                        
                        <p>Select showtime:</p>
                        ";
        // line 46
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable(($context["showTimeList"] ?? null));
        foreach ($context['_seq'] as $context["_key"] => $context["showtimes"]) {
            // line 47
            echo "                             <p id=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["showtimes"], "movieShowtimeId", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["showtimes"], "time", array()), "html", null, true);
            echo "</p>                           
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['showtimes'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 48
        echo "                       
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "showtimes.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 48,  91 => 47,  87 => 46,  55 => 16,  52 => 15,  39 => 5,  36 => 4,  30 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends \"master.html.twig\" %}

{% block title %}ShowTimes{% endblock %}
{% block addhead %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$('body').on('click', function (e) {
                var showtime = e.target.id;
                window.location = \"/seatcharts\" + \"?showtime=\" + showtime;
            });
        });
    </script>
{% endblock %}
{% block content %}
    <div class=\"container\">
        <div class=\"row\">
            <div class=\"col-md-9 header\">
                <img id=\"logo\" src=\"/images/logo.jpg\" alt=\"1\">
            </div>
            <div class=\"col-md-3 header\">
                <input id=\"search\" type=\"search\" placeholder=\"Search\">
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-md-12 header\">
                <nav class=\"navbar navbar-expand-md  navbar-dark\">
                    <a class=\"navbar-brand\" href=\"#\"></a>
                    <button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">
                        <span class=\"navbar-toggler-icon\"></span></button>
                    <div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">
                        <ul class=\"navbar-nav\">
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"register.html\">SignUp</a></li>
                            <li class=\"nav-item\"><a class=\"nav-link\" href=\"login.html\">Login</a></li>
                        </ul>
                    </div>
                </nav>
            </div>
        </div>
        <div class=\"row\">
            <div class=\"col-sm-12\">
                <div class=\"card\">
                    <div class=\"card-body\">
                        
                        <p>Select showtime:</p>
                        {% for showtimes in showTimeList %}
                             <p id=\"{{showtimes.movieShowtimeId}}\">{{showtimes.time}}</p>                           
                        {% endfor %}                       
                       
                    </div>
                </div>
            </div>
        </div>
    </div>
{% endblock %}", "showtimes.html.twig", "C:\\xampp\\htdocs\\movieticketingtannaz\\templates\\showtimes.html.twig");
    }
}
