<?php

/* reserve_confirm.html.twig */
class __TwigTemplate_63747b32acd9d12b437b1b3366d0f7f23843d79d4080575be11ffaa755a3914c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'addhead' => array($this, 'block_addhead'),
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
";
        // line 2
        $this->displayBlock('title', $context, $blocks);
        // line 3
        $this->displayBlock('addhead', $context, $blocks);
        // line 13
        $this->displayBlock('content', $context, $blocks);
    }

    // line 2
    public function block_title($context, array $blocks = array())
    {
        echo "Reservation Successful";
    }

    // line 3
    public function block_addhead($context, array $blocks = array())
    {
        // line 4
        echo "    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"img\").click(function () {
                window.location = \"/pdf\";
            });
        });
    </script>
";
    }

    // line 13
    public function block_content($context, array $blocks = array())
    {
        // line 14
        echo "        movie name: <p>";
        echo twig_escape_filter($this->env, ($context["movietitle"] ?? null), "html", null, true);
        echo "</p><br>
        Cinema name:<p>";
        // line 15
        echo twig_escape_filter($this->env, ($context["cinemaname"] ?? null), "html", null, true);
        echo "</p><br>
        Auditorium: <p>";
        // line 16
        echo twig_escape_filter($this->env, ($context["auditorium"] ?? null), "html", null, true);
        echo "</p><br>
        Date:<p>";
        // line 17
        echo twig_escape_filter($this->env, ($context["showdate"] ?? null), "html", null, true);
        echo "</p><br>
        showtime:<p>";
        // line 18
        echo twig_escape_filter($this->env, ($context["showtime"] ?? null), "html", null, true);
        echo "</p><br>
        seat id: <p>";
        // line 19
        echo twig_escape_filter($this->env, ($context["seatnumber"] ?? null), "html", null, true);
        echo "</p><br>
        User Name:<p></p><br>
<img id=\"print\" src=\"/images/printer.png\" alt=\"print\" >

";
    }

    public function getTemplateName()
    {
        return "reserve_confirm.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  78 => 19,  74 => 18,  70 => 17,  66 => 16,  62 => 15,  57 => 14,  54 => 13,  42 => 4,  39 => 3,  33 => 2,  29 => 13,  27 => 3,  25 => 2,  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("
{% block title %}Reservation Successful{% endblock %}
{% block addhead %}
    <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js\"></script>
    <script>
        \$(document).ready(function () {
            \$(\"img\").click(function () {
                window.location = \"/pdf\";
            });
        });
    </script>
{% endblock %}
{% block content %}
        movie name: <p>{{movietitle}}</p><br>
        Cinema name:<p>{{cinemaname}}</p><br>
        Auditorium: <p>{{auditorium}}</p><br>
        Date:<p>{{showdate}}</p><br>
        showtime:<p>{{showtime}}</p><br>
        seat id: <p>{{seatnumber}}</p><br>
        User Name:<p></p><br>
<img id=\"print\" src=\"/images/printer.png\" alt=\"print\" >

{% endblock %}
", "reserve_confirm.html.twig", "C:\\xampp\\htdocs\\movieticketingtannaz\\templates\\reserve_confirm.html.twig");
    }
}
