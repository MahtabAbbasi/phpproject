<?php

require_once 'header.php';

// These lines (into if) are not executed. 
// They are only for show and use features of statement completion.
if(false) 
{
    require_once 'vendor/autoload.php';
    
    $app = new \Slim\Slim(array(
            'view' => new \Slim\Views\Twig()
    ));
    
    $log = new Monolog\Logger('main');
}

$app->get('/', function() use ($app, $log) 
{
    $_SESSION['login']   = 0;
    $_SESSION['section'] = 0;
    
    $nowPlayingList= array();
    $comingSoonList = array();

    $moviesList = DB::query("SELECT * FROM movies");
    $_SESSION['movies'] = $moviesList;
        
    $m = array();
    $cinemasList = array();
    foreach($moviesList as $m)
    {
        $cinemaList1 = DB::query("SELECT cinemas.name, movieshowtimes.movieId FROM "
                . "movieshowtimes JOIN auditoriums on movieshowtimes.AuditoriumId = auditoriums.AudId "
                . "JOIN cinemas on cinemas.cinemaId = auditoriums.cinemaId "
                . "WHERE movieshowtimes.movieId=%i GROUP BY cinemas.cinemaId", $m['movieId']);
                
        array_push($cinemasList, $cinemaList1);
    }
    $_SESSION['cinemas'] = $cinemasList;
					    
    $cinemaList = DB::query("SELECT cinemas.cinemaId, cinemas.name, auditoriums.AudId"
            . " FROM auditoriums JOIN cinemas on "
            . "cinemas.cinemaId = auditoriums.cinemaId GROUP BY cinemas.cinemaId");
    $_SESSION['cinema'] = $cinemaList;
    
    unset($m);
    foreach ($moviesList as $m)
    {
        if($m['status'] == "Now Playing")
            array_push ($nowPlayingList, $m);
        else 
            array_push ($comingSoonList, $m);
    }
    $_SESSION['playing'] = $nowPlayingList;
    $_SESSION['coming']  = $comingSoonList;

    $log->debug('preparing to send movie list to index html.');
    
    $app->render('index.html.twig');
});

$app->post('/', function() use ($app, $log) 
{
    $id = $app->request()->post('selectedmovie');

    $selectedCinemaName = $app->request()->post('selectedcinema');
    $selectedCinema = DB::queryFirstRow("SELECT * FROM cinemas WHERE cinemas.name=%s", $selectedCinemaName);
    $_SESSION['cinema'] = $selectedCinema;
    
    $selectedDate = $app->request()->post('date');
    $_SESSION['date'] = $selectedDate;
    
    $showTimeList = DB::query("SELECT * FROM movieshowtimes JOIN auditoriums on "
                    . "movieshowtimes.AuditoriumId = auditoriums.AudId JOIN "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId JOIN "
                    . "movies on movies.movieId = movieshowtimes.movieId "
                    . "WHERE movies.movieId=%s and cinemas.cinemaId = %s and movieshowtimes.date = %s"
                    , $id, $selectedCinema['cinemaId'], $selectedDate);
    $_SESSION['show'] = $showTimeList;
        
    $app->render('showtimes1.html.twig');
});

$app->get('/:login', function($login) use ($app, $log) 
{
    $_SESSION['login']   = $login;
    $_SESSION['section'] = 0;
    
    $nowPlayingList= array();
    $comingSoonList = array();

    $moviesList = DB::query("SELECT * FROM movies");
    $_SESSION['movies'] = $moviesList;
        
    $m = array();
    $cinemasList = array();
    foreach($moviesList as $m)
    {
        $cinemaList1 = DB::query("SELECT cinemas.name, movieshowtimes.movieId FROM "
                . "movieshowtimes JOIN auditoriums on movieshowtimes.AuditoriumId = auditoriums.AudId "
                . "JOIN cinemas on cinemas.cinemaId = auditoriums.cinemaId "
                . "WHERE movieshowtimes.movieId=%i GROUP BY cinemas.cinemaId", $m['movieId']);
                
        array_push($cinemasList, $cinemaList1);
    }
    $_SESSION['cinemas'] = $cinemasList;
					    
    unset($m);
    foreach ($moviesList as $m)
    {
        if($m['status'] == "Now Playing")
            array_push ($nowPlayingList, $m);
        else 
            array_push ($comingSoonList, $m);
    }
    $_SESSION['playing'] = $nowPlayingList;
    $_SESSION['coming']  = $comingSoonList;

    $log->debug('preparing to send movie list to index html.');
    
    $app->render('index.html.twig', array(
        'login' => $login
    ));
});

$app->post('/:login', function($login) use ($app, $log) 
{
    $id = $app->request()->post('selectedmovie');

    $selectedCinemaName = $app->request()->post('selectedcinema');
    $selectedCinema = DB::queryFirstRow("SELECT * FROM cinemas WHERE cinemas.name=%s", $selectedCinemaName);
    $_SESSION['cinema'] = $selectedCinema;
    
    $selectedDate = $app->request()->post('date');
    $_SESSION['date'] = $selectedDate;
    
    $showTimeList = DB::query("SELECT * FROM movieshowtimes JOIN auditoriums on "
                    . "movieshowtimes.AuditoriumId = auditoriums.AudId JOIN "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId JOIN "
                    . "movies on movies.movieId = movieshowtimes.movieId "
                    . "WHERE movies.movieId=%s and cinemas.cinemaId = %s and movieshowtimes.date = %s"
                    , $id, $selectedCinema['cinemaId'], $selectedDate);
    $_SESSION['show'] = $showTimeList;
        
    $app->render('showtimes1.html.twig');
});

$app->get('/showtimes', function() use ($app, $log) 
{
    $app->render('showtimes.html.twig');
});

require_once 'login.php';
require_once 'movie.php';
require_once 'ticket.php';

$app->run();