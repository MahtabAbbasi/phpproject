<?php

session_start();

require_once 'vendor/autoload.php';
require_once 'fpdf/fpdf.php';

use Monolog\Logger;
use Monolog\Handler\StreamHandler;

// create a log channel
$log = new Logger('main');
$log->pushHandler(new StreamHandler('logs/everything.log', Logger::DEBUG));
$log->pushHandler(new StreamHandler('logs/errors.log', Logger::ERROR));

if(true)
{
    DB::$user = 'movieticketing';
    DB::$dbName = 'movieticketing';
    DB::$password = 'JxIRPIJDD6hmUbMx';
    DB::$port = 3333;
    DB::$host = 'localhost';
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
}
 else
{
    DB::$user = 'movieticketing';
    DB::$dbName = 'movieticketing';
    DB::$password = 'JxIRPIJDD6hmUbMx';
    DB::$encoding = 'utf8';
    DB::$error_handler = 'db_error_handler';
}
    
function db_error_handler($params) 
{
    global $app, $log;
    $log->error("SQL error: " . $params['error']);
    $log->error("SQL query: " . $params['query']);
    http_response_code(500);
    $app->render('fatal_error.html.twig');
    die; // don't want to keep going if a query broke
}

// Slim creation and setup
$app = new \Slim\Slim(array(
    'view' => new \Slim\Views\Twig()
));

$view = $app->view();

$view->parserOptions = array(
    'debug' => true,
    'cache' => dirname(__FILE__) . '/cache'
);

$view->setTemplatesDirectory(dirname(__FILE__) . '/templates');

if(!isset($_SESSION['user']))
{
    $_SESSION['user'] =array();
}

\Slim\Route::setDefaultConditions(array(
    'id'    => '\d+',
    'login' => '\d+'
));

$twig = $app->view()->getEnvironment();
$twig->addGlobal('sessionUser'   , $_SESSION['user']);
$twig->addGlobal('sessionLogin'  , $_SESSION['login']);
$twig->addGlobal('sessionSection', $_SESSION['section']);
$twig->addGlobal('sessionMovies' , $_SESSION['movies']);
$twig->addGlobal('sessionCinema' , $_SESSION['cinema']);
$twig->addGlobal('sessionCinemas', $_SESSION['cinemas']);
$twig->addGlobal('sessionPlaying', $_SESSION['playing']);
$twig->addGlobal('sessionComing' , $_SESSION['coming']);
$twig->addGlobal('sessionShow'   , $_SESSION['show']);
