<?php

// These lines (into if) are not executed. 
// They are only for show and use features of statement completion.
if(false) 
{
    require_once 'vendor/autoload.php';
    
    $app = new \Slim\Slim(array(
            'view' => new \Slim\Views\Twig()
    ));
    
    $log = new Monolog\Logger('main');
}

$app->get('/seatcharts', function() use ($app) 
{
    $showtimeId = $app->request()->get('showtime');
    $showtime = DB::queryFirstRow("SELECT * FROM movieshowtimes WHERE movieshowtimes.movieShowtimeId=%s ", $showtimeId);
    $_SESSION['showtime'] = $showtime;
    $auditorium = DB::queryFirstRow("SELECT * FROM auditoriums join "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId"
                    . " WHERE cinemas.cinemaId = %s", $_SESSION['cinema']['cinemaId']);
    $_SESSION['auditorium'] = $auditorium;
    $audRows = DB::query("SELECT * FROM audrows JOIN "
                    . "movieshowtimes on movieshowtimes.movieShowtimeId=audrows.showtimeid "
            . "WHERE movieshowtimes.movieShowtimeId=%s"
                    , $showtime['movieShowtimeId']);
    $app->render('seatchart.html.twig', array('auditorium' => $auditorium, 'audRows'=> $audRows));
});

$app->get('/ticketdetails', function() use ($app) {

    $selectedseat = $app->request()->get('seatNumber');
    $_SESSION['selectedseat'] = $selectedseat;
    $app->render('ticketdetails.html.twig', array(
        'movietitle' => $_SESSION['movie']['title'],
        'cinemaname' => $_SESSION['cinema']['name'],
        'auditorium' => $_SESSION['auditorium']['location'],
        'showdate' => $_SESSION['showtime']['date'],
        'showtime' => $_SESSION['showtime']['time'],
        'seatnumber' => substr_replace($selectedseat, "", -1)
    ));
});

$app->post('/ticketdetails', function() use ($app) {
    $myArray = explode(",", substr_replace($_SESSION['selectedseat'], "", -1));
    $row = $myArray[0];
    $seat = $myArray[1];
    $Row = DB::queryFirstRow("SELECT * FROM audrows WHERE rowNumber=%s AND showtimeid=%s", $row,$_SESSION['showtime']['movieShowtimeId']);
    $seatInRow = $Row['seatsInRow'];
    $seats = explode(",", $seatInRow);
    $updatedSeatsInRowArray = array();
    //generate the new seats in row string
    for($i=0;$i<count($seats);$i++){
       if($i == ($seat--)){
           $updatedSeatsInRowArray[$i] = "0";
       }else{
           $updatedSeatsInRowArray[$i] = $seats[$i];
       }
    }
    $updatedSeatsInRow = implode(",", $updatedSeatsInRowArray); 
    //Update the seat chart
    DB::update('audrows', array('seatsInRow' => $updatedSeatsInRow), ' rowId=%i', $Row['rowId']);
    $_SESSION['seatRow'] = $row;
    $_SESSION['seatN'] = $seat;
    
    //Insert ticket in the database
    DB::insert('tickets', array(
        'movieShowtimeId' => $_SESSION['showtime']['movieShowtimeId'],
        'seatNumber' => $_SESSION['seatN'],
        'rowId' => $_SESSION['seatRow'],
            //'userId' => ''
    ));
    $app->render('reserve_confirm.html.twig', array(
        'movietitle' => $_SESSION['movie']['title'],
        'cinemaname' => $_SESSION['cinema']['name'],
        'auditorium' => $_SESSION['auditorium']['location'],
        'showdate' => $_SESSION['showtime']['date'],
        'showtime' => $_SESSION['showtime']['time'],
        'seatnumber' => substr_replace($_SESSION['selectedseat'], "", -1)
    ));
});

$app->get('/pdf', function() use ($app) {

    $pdf = new FPDF();
    $pdf->AddPage();
    $pdf->Rect(20, 20, 170, 170, 'B');
    $pdf->SetFont('Arial', 'B', 18);
    $pdf->SetXY(80, 20);
    $pdf->Cell(80, 20, 'Ticket Information', 'C');
    $pdf->Ln(10);
    $pdf->Cell(50, 20, 'Name:', 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 20, 'Movie:' . $_SESSION['movie']['title'], 5, 20, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Cinema:' . $_SESSION['cinema']['name'], 5, 5, 'C');
    $pdf->Ln(10);
    //$pdf->SetX(45);
    $pdf->Cell(50, 50, 'Auditorium: ' . $_SESSION['auditorium']['location'], 5, 5, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Date: ' . $_SESSION['showtime']['date'], 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Show time: ' . $_SESSION['showtime']['time'], 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->SetX(45);
    $pdf->Cell(50, 50, 'Seat Number: ' . substr_replace($_SESSION['selectedseat'], "", -1), 5, 0, 'C');
    $pdf->Ln(10);
    $pdf->Output();
    exit;
});
