<?php

// These lines (into if) are not executed. 
// They are only for show and use features of statement completion.
if(false) 
{
    require_once 'vendor/autoload.php';
    
    $app = new \Slim\Slim(array(
            'view' => new \Slim\Views\Twig()
    ));
    
    $log = new Monolog\Logger('main');
}

$app->get('/login', function() use ($app, $log) 
{
    $_SESSION['login'] = 0;
    $app->render('login.html.twig');
});

$app->post('/login', function() use ($app, $log) 
{
    // receiving submission
    $email     = $app->request()->post('email');
    $password  = $app->request()->post('password');

    $log->debug('check login in login post.');
    
    // verify submission
    $isLoginSuccessful = false;

    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);    

    if ($user && ($user['password'] == $password))
        $isLoginSuccessful = true;
                
    //
    if ($isLoginSuccessful) 
    {
        // state 2: successful submission
        unset($user['password']);
        $_SESSION['user'] = $user;
        if($_SESSION['section'] == 0)
        {
            $level = $user['level'];
            if($level == "admin")
                $app->render('addmovies1.html.twig');
            else 
                $app->render('login_success.html.twig');
        }
        elseif($_SESSION['section'] == 1)
            $app->render('index.html.twig'); // change to payment section
    } 
    else 
    {
        // state 3: failed submission
        $app->render('login.html.twig', array('error' => true));
    }
});

$app->get('/addmovies', function() use ($app, $log) 
{
    // state 1: first show
    $app->render('addmovies.html.twig');
});

$app->post('/addmovies', function() use ($app, $log) 
{
    $movieTitle   = $app->request()->post('movieTitle');
    $movieDesc    = $app->request()->post('desc');
    $imagePath    = $app->request()->post('image');
    $backDropPath = $app->request()->post('backdrop');
    $userRate     = $app->request()->post('vote');
    $movieGenre   = $app->request()->post('genre');
    $category     = $app->request()->post('category');
    $duration     = $app->request()->post('duration');

    $date         = $app->request()->post('date');
    $time         = $app->request()->post('time');
    $status       = $app->request()->post('selectedstatus');
    $cinemaName   = $app->request()->post('selectedcinema');
    $cinemaId     = $app->request()->post('');
    $auditoriumId = $app->request()->post('selectedaudiorium');

    $movieList = array('date' => $date, 'time' => $time);
    
    var_dump($movieList);
    var_dump($movieDesc);

    // verify submission
    $errorList = array();

    $movie1 = DB::queryFirstRow("SELECT * FROM movies WHERE title=%s", $movieTitle);    

    if ($movie1)
        array_push($errorList, "This movie already exists in the movies list.");
                
    //
    if (!$errorList) 
    {
//        DB::$error_handler = FALSE;
//        DB::$throw_exception_on_error = TRUE;
        // state 2: successful submission
//        try 
//        {
//            DB::startTransaction();
            
            DB::insert('movies', array(
                'title'       => $movieTitle,
                'description' => $movieDesc,
                'category'    => $movieGenre, 
                'movieRating' => $category,
                'userRating'  => $userRate,
                'duration '   => $duration,
                'teaser '     => $imagePath,
                'status '     => $status
            ));
            
            $movieId = DB::insertId();
            
            $cinemaId = DB::queryFirstRow("SELECT * FROM cinemas WHERE name=%s", $cinemaName);    
            
            DB::insert('movieshowtimes ', array(
                'movieId'      => $movieId,
                'cinemaId'     => $cinemaId,
                'date'         => $date,
                'time'         => $time, 
                'AuditoriumId' => $auditoriumId,
            ));

            $moviesList = DB::query("SELECT * FROM movies");
            $_SESSION['movies'] = $moviesList;
            
            $m = array();
            $cinemasList = array();
            foreach($moviesList as $m)
            {
                $cinemaList1 = DB::query("SELECT cinemas.name, movieshowtimes.movieId FROM "
                        . "movieshowtimes JOIN auditoriums on movieshowtimes.AuditoriumId = auditoriums.AudId "
                        . "JOIN cinemas on cinemas.cinemaId = auditoriums.cinemaId "
                        . "WHERE movieshowtimes.movieId=%i GROUP BY cinemas.cinemaId", $m['movieId']);
                array_push($cinemasList, $cinemaList1);
            }
            $_SESSION['cinemas'] = $cinemasList;

            $app->render('signup_success.html.twig');
//        } 
//        catch (MeekroDBException $e) 
//        {
//            DB::rollback();
//            sql_error_handler(array(
//                'error' => $e->getMessage(),
//                'query' => $e->getQuery()
//            ));
//        }
    } 
    else 
    {
        // state 3: failed submission
        $app->render('addmovies.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList));
    }
});

$app->get('/isemailregistered/(:email)', function($email = "") use ($app, $log) 
{
    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);
    echo ($user) ? "Email already in use" : "";
});

$app->get('/signup', function() use ($app, $log) 
{
    // state 1: first show
    $app->render('signup.html.twig');
});

$app->post('/signup', function() use ($app, $log) 
{
    // receiving submission
    $email     = $app->request()->post('email');
    $firstName = $app->request()->post('firstName');
    $lastName  = $app->request()->post('lastName');
    $phoneNo   = $app->request()->post('phoneNo');
    $password  = $app->request()->post('password');
    $password1 = $app->request()->post('password1');

    $valueList = array('email' => $email, 'firstName' => $firstName, 
        'lastName' => $lastName, 'phoneNo' => $phoneNo);
    
    // verify submission
    $errorList = array();

    $user = DB::queryFirstRow("SELECT * FROM users WHERE email=%s", $email);    

    if ($user)
        array_push($errorList, "This Email address already exists.");
    else
    {    
        if (strlen($firstName) < 2 || strlen($firstName) > 50)
            array_push($errorList, "First name must be 2-50 characters long");

        if (strlen($lastName) < 2 || strlen($lastName) > 50)
            array_push($errorList, "Last name must be 2-50 characters long");

        if (filter_var($email, FILTER_VALIDATE_EMAIL) === FALSE)
        {
            array_push($errorList, "Email is invalid.");
            unset($valueList['email']);
        }
            
        if ($password != $password1 || $password == "") 
            array_push($errorList, "Passwords must be identical and not empty.");
        elseif (strlen($password) < 6 || strlen($password) > 100)
            array_push($errorList, "Password must be at least 6 characters long.");
        else
        {
            if ((preg_match('/[a-z]/', $password) != 1) || 
                (preg_match('/[A-Z]/', $password) != 1) ||
                (preg_match('/[0-9]/', $password) != 1)) 
                    array_push($errorList, "Password must contain at least one "
                            . "uppercase, one lowercase letter and at least one "
                            . "digit.");
        }
    }
                
    //
    if (!$errorList) 
    {
        DB::$error_handler = FALSE;
        DB::$throw_exception_on_error = TRUE;
        // state 2: successful submission
//        try 
//        {
//            DB::startTransaction();
            
            DB::insert('users', array(
                'email'       => $email,
                'password'    => $password,
                'firstName'   => $firstName, 
                'lastName'    => $lastName,
                'phoneNumber' => $phoneNo
            ));
            $app->render('signup_success.html.twig');
//        } 
//        catch (MeekroDBException $e) 
//        {
//            DB::rollback();
//            sql_error_handler(array(
//                'error' => $e->getMessage(),
//                'query' => $e->getQuery()
//            ));
//        }
    } 
    else 
    {
        // state 3: failed submission
        $app->render('signup.html.twig', array(
            'v' => $valueList,
            'errorList' => $errorList));
    }
});

$app->get('/logout', function() use ($app, $log) 
{
    $_SESSION['login'] = "true";
    
    $_SESSION['user'] = array();

    $nowPlayingList= array();
    $comingSoonList = array();

    $userList = $_SESSION['user'];
        
    $movieList = DB::query("SELECT * FROM movies");
    $_SESSION['movies'] = $movieList;
        
    $m = array();
    $cinemaList = array();
    foreach($movieList as $m)
    {
        $cinemaList1 = DB::query("SELECT cinemas.name, movieshowtimes.movieId FROM "
                . "movieshowtimes JOIN auditoriums on movieshowtimes.AuditoriumId = auditoriums.AudId "
                . "JOIN cinemas on cinemas.cinemaId = auditoriums.cinemaId "
                . "WHERE movieshowtimes.movieId=%i GROUP BY cinemas.cinemaId", $m['movieId']);
                
        array_push($cinemaList, $cinemaList1);
    }
					    
    unset($m);
    foreach ($movieList as $m)
    {
        if($m['status'] == "Now Playing")
            array_push ($nowPlayingList, $m);
        else 
            array_push ($comingSoonList, $m);
    }

    $log->debug('preparing to send movie list to index html.');
    
    $app->render('logout.html.twig');
});
