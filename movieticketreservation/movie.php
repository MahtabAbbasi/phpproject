<?php

// These lines (into if) are not executed. 
// They are only for show and use features of statement completion.
if(false) 
{
    require_once 'vendor/autoload.php';
    
    $app = new \Slim\Slim(array(
            'view' => new \Slim\Views\Twig()
    ));
    
    $log = new Monolog\Logger('main');
}

$app->get('/movieDescription/:id', function($id) use ($app, $log) 
{
    foreach($_SESSION['movies'] as $m)
    {
        if($m['movieId'] == $id)
        {
            $movieList = $m;
            $_SESSION['movie'] = $m;
            break;
        }
    }

    $m = array();
    $cinemaList = array();
    foreach($_SESSION['movies'] as $m)
    {
        $cinemaList1 = DB::query("SELECT cinemas.name, movieshowtimes.movieId FROM "
                . "movieshowtimes JOIN auditoriums on movieshowtimes.AuditoriumId = auditoriums.AudId "
                . "JOIN cinemas on cinemas.cinemaId = auditoriums.cinemaId "
                . "WHERE movieshowtimes.movieId=%i GROUP BY cinemas.cinemaId", $m['movieId']);
                
        array_push($cinemaList, $cinemaList1);
    }
    
    $app->render('moviedescription.html.twig', array(
        'cinemaList'  => $cinemaList,
        'movie'   => $movieList
    ));
});

$app->post('/movieDescription/:id', function($id) use ($app, $log) {
    $selectedCinemaName = $app->request()->post('selectedcinema');

    $selectedCinema = DB::queryFirstRow("SELECT * FROM cinemas WHERE cinemas.name=%s", $selectedCinemaName);
    $_SESSION['cinema'] = $selectedCinema;
    
    $selectedDate = $app->request()->post('date');
    $_SESSION['date'] = $selectedDate;
    
    $showTimeList = DB::query("SELECT * FROM movieshowtimes JOIN auditoriums on "
                    . "movieshowtimes.AuditoriumId = auditoriums.AudId JOIN "
                    . "cinemas on cinemas.cinemaId = auditoriums.cinemaId JOIN "
                    . "movies on movies.movieId = movieshowtimes.movieId "
                    . "WHERE movies.movieId=%s and cinemas.cinemaId = %s and movieshowtimes.date = %s"
                    , $id, $selectedCinema['cinemaId'], $selectedDate);
    $_SESSION['show'] = $showTimeList;
        
    $app->render('showtimes1.html.twig');
});

