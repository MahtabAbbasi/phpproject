-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1:3333
-- Generation Time: Jan 23, 2019 at 08:25 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `movieticketing`
--

-- --------------------------------------------------------

--
-- Table structure for table `auditoriums`
--

CREATE TABLE `auditoriums` (
  `AudId` int(11) NOT NULL,
  `cinemaId` int(11) NOT NULL,
  `location` varchar(50) NOT NULL,
  `numberSeats` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `auditoriums`
--

INSERT INTO `auditoriums` (`AudId`, `cinemaId`, `location`, `numberSeats`) VALUES
(2, 1, 'second floor', 200),
(3, 3, 'first floor', 112);

-- --------------------------------------------------------

--
-- Table structure for table `audrows`
--

CREATE TABLE `audrows` (
  `rowId` int(11) NOT NULL,
  `audId` int(11) NOT NULL,
  `showtimeid` int(11) NOT NULL,
  `rowNumber` int(11) NOT NULL,
  `seatsInRow` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `audrows`
--

INSERT INTO `audrows` (`rowId`, `audId`, `showtimeid`, `rowNumber`, `seatsInRow`) VALUES
(1, 2, 2, 1, '$,1,1,1,0,0,$'),
(2, 2, 2, 2, '$,$,1,0,0,$,$'),
(3, 2, 2, 3, '$,1,1,1,1,1,$'),
(4, 2, 2, 4, '$,1,0,0,1,1,$'),
(5, 2, 1, 2, '$,$,1,1,1,$,$');

-- --------------------------------------------------------

--
-- Table structure for table `cinemas`
--

CREATE TABLE `cinemas` (
  `cinemaId` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `city` varchar(100) NOT NULL,
  `address` varchar(100) NOT NULL,
  `postalCode` varchar(10) NOT NULL,
  `phoneNumber` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cinemas`
--

INSERT INTO `cinemas` (`cinemaId`, `name`, `city`, `address`, `postalCode`, `phoneNumber`) VALUES
(1, 'Cineplex', 'Montreal', 'st.Catherine', 'H6H6H6', '5141002525'),
(2, 'Cinemas Guzzo- Cremazie', 'Montreal, QC', '901 Boulevard Cremazie O', 'H4N 3M5', '514-385-5566'),
(3, 'Cinema Banque Scotia', 'Montreal, Qc', '977 Rue Sainte-Catherine O', 'H3B 4W3', '514-842-0549');

-- --------------------------------------------------------

--
-- Table structure for table `movies`
--

CREATE TABLE `movies` (
  `movieId` int(11) NOT NULL,
  `title` varchar(100) NOT NULL,
  `description` varchar(10000) NOT NULL,
  `category` varchar(100) NOT NULL,
  `movieRating` varchar(50) DEFAULT NULL,
  `userRating` decimal(10,0) DEFAULT NULL,
  `duration` varchar(10) NOT NULL,
  `teaser` mediumblob,
  `trailer` varchar(300) DEFAULT NULL,
  `status` enum('Now Playing','Coming Soon') NOT NULL DEFAULT 'Now Playing'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `movies`
--

INSERT INTO `movies` (`movieId`, `title`, `description`, `category`, `movieRating`, `userRating`, `duration`, `teaser`, `trailer`, `status`) VALUES
(1, 'Bohemian Rhapsody 1', 'BOHEMIAN RHAPSODY is a foot-stomping celebration of Queen, their music and their extraordinary lead singer Freddie Mercury. Freddie defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet. The film traces the meteoric rise of the band through their iconic songs and revolutionary sound. They reach unparalleled success, but in an unexpected turn Freddie, surrounded by darker influences, shuns Queen in pursuit of his solo career. Having suffered greatly without the collaboration of Queen, Freddie manages to reunite with his bandmates just in time for Live Aid. Facing a life-threatening illness, Freddie leads the band in one of the greatest performances in the history of rock music. Queen cements a legacy that continues to inspire outsiders, dreamers and music lovers to this day.', 'Drama', 'G', '8', '2.15', NULL, '', 'Now Playing'),
(2, 'Bohemian Rhapsody', 'BOHEMIAN RHAPSODY is a foot-stomping celebration of Queen, their music and their extraordinary lead singer Freddie Mercury. Freddie defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet. The film traces the meteoric rise of the band through their iconic songs and revolutionary sound. They reach unparalleled success, but in an unexpected turn Freddie, surrounded by darker influences, shuns Queen in pursuit of his solo career. Having suffered greatly without the collaboration of Queen, Freddie manages to reunite with his bandmates just in time for Live Aid. Facing a life-threatening illness, Freddie leads the band in one of the greatest performances in the history of rock music. Queen cements a legacy that continues to inspire outsiders, dreamers and music lovers to this day.', 'Drama', 'G', '8', '2.15', NULL, '', 'Coming Soon'),
(3, 'Bohemian Rhapsody', 'BOHEMIAN RHAPSODY is a foot-stomping celebration of Queen, their music and their extraordinary lead singer Freddie Mercury. Freddie defied stereotypes and shattered convention to become one of the most beloved entertainers on the planet. The film traces the meteoric rise of the band through their iconic songs and revolutionary sound. They reach unparalleled success, but in an unexpected turn Freddie, surrounded by darker influences, shuns Queen in pursuit of his solo career. Having suffered greatly without the collaboration of Queen, Freddie manages to reunite with his bandmates just in time for Live Aid. Facing a life-threatening illness, Freddie leads the band in one of the greatest performances in the history of rock music. Queen cements a legacy that continues to inspire outsiders, dreamers and music lovers to this day.', 'Drama', 'G', '8', '2.15', NULL, '', 'Now Playing');

-- --------------------------------------------------------

--
-- Table structure for table `movieshowtimes`
--

CREATE TABLE `movieshowtimes` (
  `movieShowtimeId` int(11) NOT NULL,
  `movieId` int(11) NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `AuditoriumId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `movieshowtimes`
--

INSERT INTO `movieshowtimes` (`movieShowtimeId`, `movieId`, `date`, `time`, `AuditoriumId`) VALUES
(1, 1, '2019-01-01', '19:00:00', 2),
(2, 1, '2019-01-16', '15:00:00', 2),
(3, 2, '2019-01-09', '14:00:00', 3),
(4, 3, '2019-01-10', '17:00:00', 2),
(5, 1, '2019-01-08', '16:00:00', 3);

-- --------------------------------------------------------

--
-- Table structure for table `payments`
--

CREATE TABLE `payments` (
  `paymentId` int(11) NOT NULL,
  `totalPayment` double NOT NULL,
  `paymentMethod` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tickets`
--

CREATE TABLE `tickets` (
  `ticketId` int(11) NOT NULL,
  `paymentId` int(11) NOT NULL,
  `movieShowtimeId` int(11) NOT NULL,
  `seatNumber` int(11) NOT NULL,
  `rowId` int(11) NOT NULL,
  `userId` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `userId` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(20) NOT NULL,
  `firstName` varchar(50) NOT NULL,
  `lastName` varchar(50) NOT NULL,
  `phoneNumber` varchar(15) NOT NULL,
  `level` enum('user','admin') NOT NULL DEFAULT 'user'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`userId`, `email`, `password`, `firstName`, `lastName`, `phoneNumber`, `level`) VALUES
(1, 'aziz@can.com', 'aziz12', 'Aziz', 'Azimi', '5145555500', 'admin'),
(2, 'azimi@can.com', 'azimi12', 'Aziz', 'Azimi', '5145554501', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auditoriums`
--
ALTER TABLE `auditoriums`
  ADD PRIMARY KEY (`AudId`),
  ADD KEY `cinemaid` (`cinemaId`);

--
-- Indexes for table `audrows`
--
ALTER TABLE `audrows`
  ADD PRIMARY KEY (`rowId`),
  ADD KEY `audid` (`audId`);

--
-- Indexes for table `cinemas`
--
ALTER TABLE `cinemas`
  ADD PRIMARY KEY (`cinemaId`);

--
-- Indexes for table `movies`
--
ALTER TABLE `movies`
  ADD PRIMARY KEY (`movieId`);

--
-- Indexes for table `movieshowtimes`
--
ALTER TABLE `movieshowtimes`
  ADD PRIMARY KEY (`movieShowtimeId`),
  ADD KEY `movieId` (`movieId`),
  ADD KEY `AuditoriumId` (`AuditoriumId`) USING BTREE;

--
-- Indexes for table `payments`
--
ALTER TABLE `payments`
  ADD PRIMARY KEY (`paymentId`);

--
-- Indexes for table `tickets`
--
ALTER TABLE `tickets`
  ADD PRIMARY KEY (`ticketId`),
  ADD KEY `paymentid` (`paymentId`),
  ADD KEY `movieshowtimeid` (`movieShowtimeId`),
  ADD KEY `seatid` (`seatNumber`),
  ADD KEY `userid` (`userId`),
  ADD KEY `tickets_ibfk_3` (`rowId`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`userId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auditoriums`
--
ALTER TABLE `auditoriums`
  MODIFY `AudId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `audrows`
--
ALTER TABLE `audrows`
  MODIFY `rowId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `cinemas`
--
ALTER TABLE `cinemas`
  MODIFY `cinemaId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `movies`
--
ALTER TABLE `movies`
  MODIFY `movieId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `movieshowtimes`
--
ALTER TABLE `movieshowtimes`
  MODIFY `movieShowtimeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `payments`
--
ALTER TABLE `payments`
  MODIFY `paymentId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tickets`
--
ALTER TABLE `tickets`
  MODIFY `ticketId` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `userId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auditoriums`
--
ALTER TABLE `auditoriums`
  ADD CONSTRAINT `auditoriums_ibfk_1` FOREIGN KEY (`cinemaId`) REFERENCES `cinemas` (`cinemaId`);

--
-- Constraints for table `audrows`
--
ALTER TABLE `audrows`
  ADD CONSTRAINT `audrows_ibfk_1` FOREIGN KEY (`audId`) REFERENCES `auditoriums` (`AudId`);

--
-- Constraints for table `movieshowtimes`
--
ALTER TABLE `movieshowtimes`
  ADD CONSTRAINT `movieshowtimes_ibfk_1` FOREIGN KEY (`movieId`) REFERENCES `movies` (`movieId`),
  ADD CONSTRAINT `movieshowtimes_ibfk_2` FOREIGN KEY (`AuditoriumId`) REFERENCES `auditoriums` (`AudId`);

--
-- Constraints for table `tickets`
--
ALTER TABLE `tickets`
  ADD CONSTRAINT `tickets_ibfk_1` FOREIGN KEY (`paymentId`) REFERENCES `payments` (`paymentId`),
  ADD CONSTRAINT `tickets_ibfk_2` FOREIGN KEY (`movieShowtimeId`) REFERENCES `movieshowtimes` (`movieShowtimeId`),
  ADD CONSTRAINT `tickets_ibfk_3` FOREIGN KEY (`rowId`) REFERENCES `audrows` (`rowId`),
  ADD CONSTRAINT `tickets_ibfk_4` FOREIGN KEY (`userId`) REFERENCES `users` (`userId`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
